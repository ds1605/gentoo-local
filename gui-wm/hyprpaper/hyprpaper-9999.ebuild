EAPI=8

inherit autotools

if [[ "${PV}" == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/hyprwm/hyprpaper"
	KEYWORDS="~amd64"
fi

BDEPS="
	dev-libs/wayland-protocols
"
RDEPS="${BDEPS}"

SLOT=0

src_compile() {
	emake all
}

src_install() {
	dobin build/hyprpaper
}
